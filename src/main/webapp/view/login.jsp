<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 13.09.2020
  Time: 18:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../style/login.css">
</head>
<body>
<div class="wrapper-login">
    <div class="login">
        <form action="<%=request.getContextPath()%>/login" method="post">
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
                <input type="text" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-secondary" value="Login">
            </div>
            <p>Don't have an account? <a href="/register">Sign up now</a></p>
        </form>
    </div>

</div>
</body>
</html>
